# Standard documents

The `adn.sty` package includes the logo and loads the corresponding class customization package. 

```latex
% For example, assuming article class
\documentclass{article}

% including adn.sty will include adnarticle.sty automatically
\usepackage{adn}

%% To include the logo we can use any of the following ways
%\usepackage{graphicx} % load a package if we want to use images
%% The image adn-logo must exist
%\logo{\includegraphics[width=\linewidth]{adn-logo}}
%% Or set a logo
%\setlogo{UNICAMP}

\setlogo{UNICAMP}

% We can also add some text to go with the logo
\headertext{Coordination of Something}

% Title and author
\title{Some nice title}
\author{Some One}

\begin{document}
\maketitle

% Your info goes here
\end{document}
```
